<?php
	use conf\Authentication as Authentication;
	use conf\GestionBillet as GestionBillet;
	require('vendor/autoload.php');

	class AnonymousController extends Controller {

	    public function header() {
			$app = Controller::$app;
			$app->render('header.php',compact('app'));
	    }

	    public function footer() {
			Controller::$app->render('footer.php');
	    }

	    public function index(){
		    //Chargle le header
			$this->header();
			$billet = GestionBillet::afficherBilletIndex();
			$categorie = GestionBillet::listeCategoriesHome();
			if(isset($_COOKIE['id'])){
				$_SESSION['id'] = $_COOKIE['id'];
			}
			if(isset($_COOKIE['admin'])){
				$_SESSION['admin'] = $_COOKIE['admin'];
			}
			if(isset($_SESSION['id'])){
				if($_SESSION['id'] != -1){
					if(isset($_SESSION['admin'])){
						if($_SESSION['admin'] == true)
							Controller::$app->render('homePageAdmin.php',array('billets'=>$billet, 'categories' =>$categorie));
						else
							Controller::$app->render('homePageMember.php',array('billets'=>$billet, 'categories' =>$categorie));
					}
					else
						Controller::$app->render('homePageMember.php',array('billets'=>$billet, 'categories' =>$categorie));
				}
				else
					Controller::$app->render('homepage.php',array('billets'=>$billet, 'categories' =>$categorie));
			}
			else
				Controller::$app->render('homepage.php',array('billets'=>$billet, 'categories' =>$categorie));
			$this->footer();
	    }

	    public function inscription(){
			$this->header();
			Controller::$app->render('inscription.php');
			$this->footer();
	    }

	    public function insert_user(){
			$app = Controller::$app;
			$pseudo = $app->request->post('pseudo');
			if(strlen($pseudo) < 5){
				$app->flash('info', "Pseudo trop court, 5 caractère minimum");
				$app->redirectTo('inscription');
			}
			$pseudo =filter_var($pseudo,FILTER_SANITIZE_STRING);
			$nom = $app->request->post('nom');
			$nom = filter_var($nom,FILTER_SANITIZE_STRING);
			$prenom = $app->request->post('prenom');
			$prenom = filter_var($prenom,FILTER_SANITIZE_STRING);
			$mail = $app->request->post('mail');
			$mail = filter_var($mail,FILTER_SANITIZE_EMAIL);
			$mdp1 = $app->request->post('mdp1');
			$mdp1 = filter_var($mdp1,FILTER_SANITIZE_STRING);
			if(strlen($mdp1) < 8){
				$app->flash('info', "Mot de passe trop, court 8 caractère minimum");
				$app->redirectTo('inscription');
			}
			$mdp2 = $app->request->post('mdp2');
			$mdp2 = filter_var($mdp2,FILTER_SANITIZE_STRING);
			if(empty($pseudo) || empty($nom) || empty($prenom) || empty($mail) || empty($mdp1) || empty($mdp2)){
				$app->flash('info', "champ non rempli");
				$app->redirectTo('inscription');
	        }
			else if($mdp1 != $mdp2){
				$app->flash('info', "Les mot de passe ne correspondent pas");
				$app->redirectTo('inscription');
			}
			else if(!filter_var($mail, FILTER_VALIDATE_EMAIL) === true){
				$app->flash('info', "email non valide");
				$app->redirectTo('inscription');
			}
			else if(Authentication::pseudoDejaPris($pseudo) === true){
				$app->flash('info', "$pseudo deja pris");
				$app->redirectTo('inscription');
			}
			else if(Authentication::mailDejaUtilise($mail) === true){
				$app->flash('info', "$mail deja enregistré");
				$app->redirectTo('inscription');
			}
			else{
				$res = Authentication::inscription($pseudo,$nom,$prenom,$mail,$mdp1,$mdp2);
				$app->flash('info', "$pseudo, votre compte est crée avec succès");
				$app->redirectTo('root');
			}
	    }

	    public function connexion(){
			$this->header();
			Controller::$app->render('connexion.php');
			$this->footer();
	    }

	    public function connect(){
	    	$app = Controller::$app;
			$pseudo = $app->request->post('pseudo');
			$pseudo = filter_var($pseudo,FILTER_SANITIZE_STRING);
			$mdp = $app->request->post('mdp');
			$mdp = filter_var($mdp,FILTER_SANITIZE_STRING);
			if(empty($pseudo) || empty($mdp)){
				$app->flash('info', "champ non rempli");
				$app->redirectTo('connexion');
	        }
			$res = Authentication::authenticate($pseudo,$mdp);
			$_SESSION['id'] = $res;
			setcookie('id',$_SESSION['id'],time()+60*60*24*30);
			if($res != -1 && isset($_SESSION['id'])){
				$app->flash('info', "$pseudo, vous étes maintenant connecté");
				$app->redirectTo('root');
				
			}
			else{
				$_SESSION['id'] = -1;
				$app->flash('info', "connexion impossible");
				$app->redirectTo('connexion');
			}
	    }

	    public function consulte_billet($page){
	    	$app = Controller::$app;
	    	$_SESSION['categorieLabel'] = $app->request->post('categorie');
	    	$pageRes = GestionBillet::page_billet($_SESSION['categorieLabel'],$page);
	    	$_SESSION['page'];
	    	$app->redirectTo('page_billet/'.$page);
	    }

	    public function page_billet(){
	    	$this->header();
            Controller::$app->render('billetListe.php',array('page'=>$_SESSION['page']));
            $this->footer();
	    }

	}
