<?php
    use conf\Authentication as Authentication;
    use conf\GestionBillet as GestionBillet;
    require('vendor/autoload.php');

    class MembreController extends Controller {
    	
    	public function header() {
    		$app = Controller::$app;
    		$app->render('header.php',compact('app'));
        }

        public function footer() {
    		Controller::$app->render('footer.php');
        }

        public function deconnexion(){
        	$app = Controller::$app;
        	Authentication::logout();
            setcookie('id',-1,time()+60*60*24*30);
            setcookie('amdin',-1,time()+60*60*24*30);
        	$app->flash('info', "vous etes deconnecté à bientôt");
    		$app->redirectTo('root');
        }


        public function saisir_billet(){
            $app = Controller::$app;
            $res = GestionBillet::listeCategories();
    		$this->header();
    		Controller::$app->render('saisir_billet.php',array('categories'=>$res));
            $this->footer();
        }

        public function insert_billet(){
            $app = Controller::$app;
            $titre = $app->request->post('titre');
            $billet = $app->request->post('billet');
            $categorie = $app->request->post('categorie');
            $id = $_SESSION['id'];
            if(empty($titre) || empty($billet)){
                $app->flash('info', "champ non rempli");
                $app->redirectTo('saisir_billet');
            }
            $idCategorie = GestionBillet::getIdCategorie($categorie);
            GestionBillet::ajoutBillet($id, $idCategorie, $titre, $billet);
            $app->flash('info', "billet ajouté");
            $app->redirectTo('root');

        }

        public function listeCategories(){
            $res = GestionBillet::listeCategories();
            $app->render($res);
        }

        public function affiche_billet($id){
            $this->header();
            if(isset($_SESSION['id'])){
                if($_SESSION['id'] != -1){
                    $res = GestionBillet::afficherUnBillet($id,$_SESSION['id']);
                }
                else{
                    $res = GestionBillet::afficherUnBillet($id,-1);
                }
            }
            else{
               $res = GestionBillet::afficherUnBillet($id,-1); 
            }
            Controller::$app->render('billet.php',array('id'=>$res,'idBillet'=>$id));
            $this->footer();
        }

        public function comment_billet($id){
            $app = Controller::$app;
            $comment = $app->request->post('comment');
            $idUser = $_SESSION['id'];
            if(empty($comment)){
                $app->flash('info', "champ non rempli");
                $app->redirectTo('root');
            }
            else{
                GestionBillet::ajoutComment($id, $comment, $idUser);
                $app->flash('info', "commentaire ajouté");
                $app->redirectTo('root');
            }
        }

        public function modifier_compte(){
            $app = Controller::$app;
            $this->header();
            Controller::$app->render('modifier_compte.php');
            $this->footer();
        }

        public function maj_compte(){

            $app = Controller::$app;


            $nom = $app->request->post('nom');
            $nom = filter_var($nom,FILTER_SANITIZE_STRING);
            $prenom = $app->request->post('prenom');
            $prenom = filter_var($prenom,FILTER_SANITIZE_STRING);
            $mail = $app->request->post('mail');
            $mail = filter_var($mail,FILTER_SANITIZE_EMAIL);
            $mdp1 = $app->request->post('mdp1');
            $mdp1 = filter_var($mdp1,FILTER_SANITIZE_STRING);
            $mdp2 = $app->request->post('mdp2');
            $mdp2 = filter_var($mdp2,FILTER_SANITIZE_STRING);

            if(!empty($mdp1)){
                if(strlen($mdp1) < 8){
                    $app->flash('info', "Mot de passe trop, court 8 caractère minimum");
                    $app->redirectTo('modifier_compte');
                }
                else if($mdp1 != $mdp2){
                    $app->flash('info', "Les mot de passe ne correspondent pas");
                    $app->redirectTo('modifier_compte');
                }
            }

            if(!empty($mail)){
                if(!filter_var($mail, FILTER_VALIDATE_EMAIL) === true){
                    $app->flash('info', "email non valide");
                    $app->redirectTo('modifier_compte');
                }
                else if(Authentication::mailDejaUtilise($mail) === true){
                    $app->flash('info', "$mail deja enregistré");
                    $app->redirectTo('modifier_compte');
                }
            }
            $res = Authentication::setProfil($_SESSION['id'],$nom,$prenom,$mail,$mdp1);
            $app->flash('info', "votre compte est à jour");
            $app->redirectTo('root');
            
        }

    }
