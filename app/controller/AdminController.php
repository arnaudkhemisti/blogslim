<?php
	use conf\Authentication as Authentication;
	use conf\GestionBillet as GestionBillet;
    use conf\GestionAdmin as GestionAdmin;
	require('vendor/autoload.php');

	class AdminController extends Controller {

    	public function header() {
    		$app = Controller::$app;
    		$app->render('header.php',compact('app'));
        }

        public function footer() {
    		Controller::$app->render('footer.php');
        }

        public function gestion_cathegorie(){
            $app = Controller::$app;
			$this->header();
			Controller::$app->render('gestion_cathegorie.php');
        	$this->footer();
        }

        public function insert_cathegorie(){
            $app = Controller::$app;
            $newCathegorie = $app->request->post('cathegorie');
            $res = GestionAdmin::AjoutCathegorie($newCathegorie);
            if($res === true){
                $app->flash('info', "Cathegorie $newCathegorie ajoutée");
                $app->redirectTo('root');
            }
            else{
                $app->flash('info', "Cathegorie $newCathegorie existe deja");
                $app->redirectTo('gestion_cathegorie');   
            }
        }

        public function liste_menbre(){
            $this->header();
            $res = GestionAdmin::menbreListe();
            Controller::$app->render('liste_menbre.php',array('liste'=>$res));
            $this->footer();
        }

        public function modifier_radie(){
            $app = Controller::$app;
            $taille = count($_SESSION['listeMenber']);
            for($i = 0 ; $i < $taille ; $i++){
                GestionAdmin::setRadie($_SESSION['listeMenber'][$i], $app->request->post($_SESSION['listeMenber'][$i]));
            } 
            $app->flash('info', "Profils changés");
            $app->redirectTo('root');
        }
    }
