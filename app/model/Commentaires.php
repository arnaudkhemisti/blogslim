<?php
	namespace app\model;

	class Commentaires extends \Illuminate\Database\Eloquent\Model {
		protected $table = 'commentaires';
		protected $primaryKey = 'id';
		public $timestamps = false;
	}

?>