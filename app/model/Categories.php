<?php
	namespace app\model;

	class Categories extends \Illuminate\Database\Eloquent\Model {
		protected $table = 'categories';
		protected $primaryKey = 'id';
		public $timestamps = false;
	}

?>