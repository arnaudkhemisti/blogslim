<?php
	namespace app\model;

	class Utilisateurs extends \Illuminate\Database\Eloquent\Model {
		protected $table = 'utilisateurs';
		protected $primaryKey = 'id';
		public $timestamps = false;
	}

?>