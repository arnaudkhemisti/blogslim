public function getBillets($page) {
       AnonymousController::header();       
        // take pour prendre 20 élément, skip concerne l'offset
       $billets = Billet::orderBy('date', 'DESC')->take(20)->skip(20 * ($page - 1))->get();        
       foreach($billets as $billet) {
           // Récupération de la catégorie en rapport avec le billet
           $category = Categorie::where('id', '=', $billet['id_categorie'])->first();
           // Récuprétion du label de la catégorie
           $categoryLabel = $category->label;
           // Ajout au tableau de billet le label de la catégorie
           $billet['category_label'] = $categoryLabel;
       }
       $categories = Categorie::all();        // Récupréation du nombre de billets
       $countBillets = count(Billet::all());
       // Division par 20 arrondie à l'unité supérieure pour mettre en place le pager
       $nbPages = ceil($countBillets / 20);        
       Controller::$app->render('billet/billets.php', array(
           'billets' => $billets,
           'nbPages' => $nbPages,
           'categories' => $categories
       ));        
       AnonymousController::modals();
       AnonymousController::footer();    
}