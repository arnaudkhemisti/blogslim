﻿L'adresse du site https://webetu.iutnc.univ-lorraine.fr/www/khemsti1u/blog/

L'adresse du depot https://bitbucket.org/arnaudkhemisti/blogslim
Répartition du travail :
Céline à fait les modèles et les fonctionnalités d’adminitrateur.

Arnaud à fait les vues, le controller pour les billets, les fonctionnalités de membres de sessions, 

la fonctionnalité de connexion, les billets, les commentaires, les membres et les cookies.

Les routes et les modèles ont été faits par nous deux. 

Arnaud n’avais pas de connexion internet privé sur son lieu de stage, 
la seule connexion possible étant 
celle de son lieu de stage ou git n’était pas 
disponible, nous avons été contraint de travailler séparément 
et de tout comiter à la fin.