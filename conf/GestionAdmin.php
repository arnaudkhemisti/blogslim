<?php
    namespace conf;
    use app\model\Commentaires as Commentaires;
    use app\model\Billets as Billets;
    use app\model\Categories as Categories;
    use app\model\Utilisateurs as Utilisateurs;

    class GestionAdmin{

        public static function ajoutCathegorie($newCathegorie){
            $res = Categories::where('label','=',$newCathegorie)->get()->first();
            if(!isset($res)){
                $cathegorie = new Categories();
                $cathegorie->label = filter_var($newCathegorie,FILTER_SANITIZE_STRING);

                $cathegorie->save();
                return true;
            }
            else{
                return false;
            }

        }

        public static function menbreListe(){
            $user = Utilisateurs::orderBy('pseudo', 'ASC')->get();
            $res = '<p>';
            $res = $res.'<FORM method="post" id="statue" action="modifier_radie">';
            foreach($user as $u){
                if(strcmp($u->profil,'membre') == 0){
                    $tabId[] = $u->id;
                    $res = $res.$u->pseudo;
                    $res = $res.'<INPUT type="checkbox" name="'.$u->id;
                    if($u->radie == 1)
                        $res = $res.'"checked>';
                    else
                        $res = $res.'">';
                    $res = $res.'<br>';
                }
            }
            $res = $res.'<input type="submit" name="" value="Valider" />';
            $res = $res.'</FORM>';
            $res = $res.'</p>';
            $_SESSION['listeMenber'] = $tabId;
            return $res;
        }

        public static function setRadie($id, $radie){
            $user = Utilisateurs::find($id);
            if(strcmp($radie,'on') == 0)
                $user->radie = 1;
            else
                $user->radie = 0;
            $user->save();
        }
    }

