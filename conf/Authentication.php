<?php
    namespace conf;
    use app\model\Utilisateurs as Utilisateurs;

    class Authentication{

        public static function authenticate($pseudo, $password) {
        	$trouve = false;
            $mdp = password_hash($password, PASSWORD_BCRYPT);
            $res = Utilisateurs::where('pseudo','=',$pseudo)->orWhere('mdp','=',$mdp)->first();
            if(strcmp($res->profil,'admin') == 0){
                setcookie('admin',true,time()+60*60*24*30);
            }
            else{
                setcookie('admin',false,time()+60*60*24*30);
            }
            if(isset($res)){
                if($res->radie == 0){
                    return $res->id;
                }
                else{
                    return -1;
                }
            }
        	return -1;
        }

        //se deconnecter
        public static function logout() {
            unset($_COOKIE['admin']);
            unset($_COOKIE['id']);
    	    session_destroy();
        }
        
        //renvoi l'user
        public static function getUser($id) {
            $res = Utilisateurs::where('id','=',$id)->get()->first();
            return $res->pseudo;
        }

        // Methoe qui ajoute un utilisateur
        public static function inscription($pseudo,$nom,$prenom,$mail,$mdp1 ,$mdp2){

            $user = new Utilisateurs();
            $user->nom = $nom;
            $user->pseudo = $pseudo;
            $user->prenom = $prenom;
            $user->mail = $mail;
            $user->mdp = password_hash($mdp1, PASSWORD_BCRYPT);
            $user->profil = 1;
            $user->radie = false;

            $user->save();
            return 0;
        }

        public static function pseudoDejaPris($pseudo){
            $res = Utilisateurs::where('pseudo','=',$pseudo)->get()->first();
            if(isset($res))
                return true;
            else
                return false;
        }

        public static function mailDejaUtilise($mail){
            $res = Utilisateurs::where('mail','=',$mail)->get()->first();
            if(isset($res))
                return true;
            else
                return false;
        }

        public static function setProfil($id, $nom, $prenom, $mail, $mdp){
            
            $user = Utilisateurs::find($id);
            if(!empty($nom))
                $user->nom = $nom;
            if(!empty($prenom))
                $user->prenom = $prenom;
            if(!empty($mail))
                $user->mail = $mail;
            if(!empty($mdp))
                $user->mdp = password_hash($mdp, PASSWORD_BCRYPT);
            $user->save();
        }
    }

