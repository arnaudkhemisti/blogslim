<?php
    namespace conf;
    use app\model\Commentaires as Commentaires;
    use app\model\Billets as Billets;
    use app\model\Categories as Categories;
    use app\model\Utilisateurs as Utilisateurs;

    class GestionBillet{

        public static function listeCategories(){
            $catego = Categories::all();
            $res = '<select name="categorie" size="1">';
            foreach($catego as $c){
                $res = $res.'<option>'.$c->label.'</option>';
            }
            $res = $res.'</select>';
            return $res;
        }


        public static function listeCategoriesHome(){
            $catego = Categories::all();
            $res = '<select name="categorie" size="1">';
            $res = $res.'<option>'."toutes les categories".'</option>';
            foreach($catego as $c){
                $res = $res.'<option>'.$c->label.'</option>';
            }
            $res = $res.'</select>';
            return $res;
        }

        public static function afficherBilletIndex(){
        	$billet = Billets::orderBy('date', 'DESC')->get();
        	if(!isset($billet)){
     			return '<p>'.'Aucun billet'.'</p>';
     		}
        	$res = '';
        	$i = 0;
            //Parcourir la liste des billet
        	foreach($billet as $b){
        		if($i < 20){
                    
                    $user = Utilisateurs::find($b->id_utilisateur);
                    if($user->radie == 0){
            			$res = $res.'<table>';
            			$user = Authentication::getUser($b->id_utilisateur);
            			$res = $res.'<tr>'.'<td>'.$user.'</td>'.'</tr>';
            			$res = $res.'<tr>'.'<td>'.'<a href="billet/'.$b->id.'">'.'Titre : '.$b->titre.'</a>'.'</td>'.'</tr>';
                        if(strlen($b->message) > 30)
                            $message = substr($b->message,0,30).'...';
                        else
                            $message = $b->message;
                        $res = $res.'<tr>'.'<td>'.$message.'</td>'.'</tr>';
                        $label = GestionBillet::getLabelCategorie($b->id_categorie);
                        $res = $res.'<tr>'.'<td>'.'Categorie : '.$label.', Date : '.$b->date.'</td>'.'</tr>';
                        $comments = Commentaires::where('id_billet','=',$b->id)->get();
                        $j = 0;
                            //Parcourir la liste des commentaire du billet
                            foreach($comments as $c){
                                $user = Utilisateurs::find($c->id_utilisateur);
                                if($user->radie == 0){    
                                    $j++;
                                }
                            }
                            $res = $res.'<tr>'.'<td>'."*** Commentaires $j ***".'</td>'.'</tr>';
                            $res = $res.'</table>';
                            $res = $res.'<br>'; 
                        
            			$i++;
                    }
        		}
        		else{
        			return $res;
        		}
        	}
        	return $res;
        }

        public static function afficherUnBillet($id, $idUser){
        	$b = Billets::find($id);
        	$res ='<table>';
        	$user = Authentication::getUser($b->id_utilisateur);
        	$res = $res.'<tr>'.'<td>'.$user.'</td>'.'</tr>';
        	$res = $res.'<tr>'.'<td>'.$b->titre.'</td>'.'</tr>';
        	$res = $res.'<tr>'.'<td>'.$b->message.'</td>'.'</tr>';
        	$label = GestionBillet::getLabelCategorie($b->id_categorie);
        	$res = $res.'<tr>'.'<td>'.'Categorie : '.$label.', Date : '.$b->date.'</td>'.'</tr>';
        	$res = $res.'</table>';
                    $comments = Commentaires::where('id_billet','=',$b->id)->get();
                        $j = 0;
                        //Parcourir la liste des commentaire du billet
                        foreach($comments as $c){
                            $user = Utilisateurs::find($c->id_utilisateur);
                            if($user->radie == 0){
                                $res = $res.'<table>';
                                $res = $res.'<tr>'.'<td>'."*** Commentaires $j ***".'</td>'.'</tr>';
                                $user = Authentication::getUser($c->id_utilisateur);
                                $res = $res.'<tr>'.'<td>'.'auteur '.$user.'</td>'.'</tr>';
                                $res = $res.'<tr>'.'<td>'.$c->message.'</td>'.'</tr>';
                                $res = $res.'<tr>'.'<td>'.$c->date.'</td>'.'</tr>';
                                $res = $res.'</table>';
                                $j++;
                            }
                        }
        	if($idUser != -1){
                $res = $res.'<br>';
                $res = $res.'<p>';
                $res = $res.'<h2>Commenter</h2>';
                $res = $res.'<form method="post" id="comment_billet" action="comment_billet/'.$id.'">';
                $res = $res.'<textarea name="comment" row=4 cols=40 placeholder="Entrez votre commentaire..." ></textarea>';
                $res = $res.'<input type="submit" name="" value="Valider" />';
                $res = $res.'</form>';
                $res = $res.'</p>';
            }
        	return $res;
        }

        public static function getLabelCategorie($id) {
            $res = Categories::where('id','=',$id)->get()->first();
            return $res->label;
        }

        public static function getIdCategorie($label) {
            $res = Categories::where('label','=',$label)->get()->first();
            return $res->id;
        }

        public static function ajoutBillet($id, $idCategorie, $titre, $bil){
     		$billet = new Billets();
     		$billet->message = $bil;
     		$billet->titre = $titre;
     		$billet->id_utilisateur = $id;
     		$billet->id_categorie = $idCategorie;

     		$billet->save();
        }

        public static function ajoutComment($idBillet,$texte,$idUser){
            $commentaire = new Commentaires();
            $commentaire->message = $texte;
            $commentaire->id_billet = $idBillet;
            $commentaire->id_utilisateur = $idUser;

            $commentaire->save();
        }
    }

